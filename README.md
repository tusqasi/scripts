<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/
font-awesome/5.15.2/css/all.min.css"/> 

# Life hack scripts


Just a few scripts to make your life easier 🤟

# 📜 project_start (🚧 WIP)
This script will take a branch name and drop you into that branch's worktree with all the file open in neovim, and also run any build command need to be run.

TODO:
	1. Add ability to open multiple branches at once
	2. Work with different project or make different scrips for different projects
	3. More may come.


## 🔋 battery_notify.py 🗑
Notifies the user when battery is overcharging or when it is draining.

## 📋 clone_from_clipboard.py 🗑
Clones a git repo into a folder on a keypress.

## 🚀 deploy (🚧 WIP)
Initializes a new distro with all the necessary programs and basic setup.

## 📓 edit_configs.sh
Opens a prompt to select configuration files and open in text editor.

## 📋 get_clip_board.py
Select a clipboard entry from history with rofi

## ⌨ keyboard configuration scripts

A few scripts to configure keyboard behaviour   
- keyboard_setup
	+ This script makes `Caps` into `Esc` when tapped and `Ctrl` when held down. Very useful.
- invertedmaps 🗑
- invert-num 🗑 
- normalmaps 🗑

## 🔍 ocr
Read text from screen to clipboard.  
[Originally from sdushantha](https://github.com/sdushantha/), I made it work for me.
## 🖥 rofi_tmux.py
Open a terminal windows with selected tmux session.

##  sec_scr.sh 
A script to use a second laptop as second screen.

## ⚙ startup
Basic startup applications.

## 🛠 utils.py
Some python functions to make scripting easier.
